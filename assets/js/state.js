//-----------------------------//
//----------- Config ----------//
//-----------------------------//

function getConfig() {
    return { ...document.libreBlobsState.config }
}

function setConfig(config) {
    document.libreBlobsState.config = {...config}
}

//-----------------------------//
//-------- Current blob -------//
//-----------------------------//

function getCurrentBlob() {
    return { ...document.libreBlobsState.currentBlob }
}

function setCurrentBlob(blob) {
    document.libreBlobsState.currentBlob = JSON.parse(JSON.stringify(blob))
}

function setCurrentBlobFromBlobs(index){

    // If a blob is currently selected
    let config = getConfig()
    if( config.selectedBlobIndex > -1){
        // Update blobs array with current blob
        updateBlobByIndex(config.selectedBlobIndex,getCurrentBlob())
    }

    // Load new blob from blobs array
    setCurrentBlob(getBlobByIndex(index))

    // Update config
    config.selectedBlobIndex = index
    setConfig(config)

    return getCurrentBlob()
}

//-----------------------------//
//-------- Blobs array --------//
//-----------------------------//

function getBlobs() {
    return [ ...document.libreBlobsState.savedBlobs ]
}

function setBlobs(blobs) {
    document.libreBlobsState.savedBlobs = JSON.parse(JSON.stringify(blobs))
}

function updateBlobByIndex(index, blob) {
    document.libreBlobsState.savedBlobs[index] =  JSON.parse(JSON.stringify(blob))
}

function getBlobByIndex(index) {
    return {...document.libreBlobsState.savedBlobs[index]}
}

function addBlob(blob) {
    let blobs = getBlobs()
    blobs.push(blob)
    setBlobs(blobs)
}

function deleteBlob(index) {
    let blobs = getBlobs()
    blobs.splice(index,1)
    setBlobs(blobs)
}

//-----------------------------//
//------- Raphael object ------//
//-----------------------------//

function getRaphaelObj() {
    // console.log(document.libreBlobsState.raphaelObj)
    return { ...document.libreBlobsState.raphaelObj }
}

function setRaphaelObj(obj) {
    document.libreBlobsState.raphaelObj = obj
}

//-----------------------------//
//----- Current background ----//
//-----------------------------//

function getCurrentBackground() {
    return { ...document.libreBlobsState.currentBackground }
}

function setCurrentBackground(bg) {
    document.libreBlobsState.currentBackground = JSON.parse(JSON.stringify(bg))
}

//-----------------------------//
//-------- State object -------//
//-----------------------------//

function getState() {
    // console.log(document.libreBlobsState.raphaelObj)
    return { ...document.libreBlobsState }
}

function setState(obj=null) {
    if(obj){
        document.libreBlobsState = obj
    } else {
        document.libreBlobsState = {
            currentBlob: {},
            savedBlobs: [],
            raphaelObj: {},
            currentBackground: {},
            config: {}
        }
    }
}

// Store relevant state in sessionStorage
function storeState() {
    // let stateToStore = {savedBlobs: getBlobs(),config: getConfig()}
    let stateToStore = {savedBlobs: getBlobs(),config: getConfig()}
    sessionStorage.setItem('LibreBlobState', JSON.stringify(stateToStore))
}

// Restore state from sessionStorage
function restoreState() {
    // If no data in session storage
    if(!sessionStorage.getItem('LibreBlobState')){
        // Create state variable
        setState()
    } else {
        // Otherwise load from session storage
        setState(JSON.parse(sessionStorage.getItem('LibreBlobState')))
    }

}